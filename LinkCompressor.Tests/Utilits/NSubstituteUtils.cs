﻿using System.Collections.Generic;
using System.Linq;
using LinkCompressor.Infrastructure.Data;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using NSubstitute;

namespace LinkCompressor.Tests.Utilits
{
	public static class NSubstituteUtils
	{
		public static void AddToDbSet<TEntity>(this DbContext context, IEnumerable<TEntity> queryableEnumerable) where TEntity : class
		{
			var set = queryableEnumerable.GenerateMockDbSet();
			context.Set<TEntity>().Returns(set);
		}

		public static DbSet<TEntity> GenerateMockDbSet<TEntity>(this IEnumerable<TEntity> queryableEnumerable) where TEntity : class
		{
			var queryable = queryableEnumerable as IQueryable<TEntity> ?? queryableEnumerable.AsQueryable();

			var mockSet = Substitute.For<DbSet<TEntity>, IQueryable<TEntity>>();
			var castMockSet = (IQueryable<TEntity>)mockSet;

			castMockSet.Provider.Returns(queryable.Provider);
			castMockSet.Expression.Returns(queryable.Expression);
			castMockSet.ElementType.Returns(queryable.ElementType);
			castMockSet.GetEnumerator().Returns(queryable.GetEnumerator());
			return mockSet;
		}
	}
}