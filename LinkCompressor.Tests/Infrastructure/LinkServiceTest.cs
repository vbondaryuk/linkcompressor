﻿using System.Collections.Generic;
using System.Linq;
using LinkCompressor.Domain.Models;
using LinkCompressor.Domain.Repositories;
using LinkCompressor.Infrastructure.Data;
using LinkCompressor.Infrastructure.Data.Repositories;
using LinkCompressor.Infrastructure.Services;
using LinkCompressor.Tests.Utilits;
using Microsoft.EntityFrameworkCore;
using NSubstitute;
using NUnit.Framework;

namespace LinkCompressor.Tests.Infrastructure
{
	[TestFixture]
	public class LinkServiceTest
    {
	    private ILinkRepository _linkRepository;
	    private IUserRepository _userRepository;
	    private ILinkCompressService _linkCompressService;
	    private LinkService _linkService;

	    [SetUp]
	    public void SetUp()
	    {
		    var dbContextOptions = new DbContextOptions<LinkCompressorContext>();
		    var mockContext = Substitute.For<LinkCompressorContext>(dbContextOptions);

		    var links = new List<Link> {new Link {ShortUrl = "tkf", OriginalUrl = "https://www.tinkoff.ru/"}};
		    mockContext.AddToDbSet(links);

		    var users = new List<User>
		    {
			    new User
			    {
				    Ip = "192.168.0.1",
				    Links = new List<Link> {new Link {ShortUrl = "tkf", OriginalUrl = "https://www.tinkoff.ru/"}}
			    }
		    };
		    mockContext.AddToDbSet(users);

			_linkRepository = Substitute.For<LinkRepository>(mockContext);
		    _userRepository = Substitute.For<UserRepository>(mockContext);
		    _linkCompressService = Substitute.For<ILinkCompressService>();
		    _linkService = new LinkService(_linkCompressService, _linkRepository, _userRepository);
		}

		[Test]
		[TestCase("qweeew")]
	    public void GetNonExistsCompressedLinkShouldBeNull(string compressedUrl)
		{
			var link = _linkService.GetLinkAndIncreaseUsages(compressedUrl);
			Assert.IsNull(link);
		}

	    [Test]
	    [TestCase("tkf")]
	    public void GetExistsCompressedLinkShouldBeIncreaseUsages(string compressedUrl)
	    {
		    var link = _linkService.GetLinkAndIncreaseUsages(compressedUrl);
		    _linkRepository.Received().Edit(Arg.Is<Link>(x => x.CountUsages == 1));
	    }

	    [Test]
	    [TestCase("tkf")]
	    public void GetExistsCompressedLinkShouldBeNotNull(string compressedUrl)
	    {
		    var link = _linkService.GetLinkAndIncreaseUsages(compressedUrl);
		    Assert.That(link.OriginalUrl, Is.EqualTo("https://www.tinkoff.ru/"));
	    }

		[Test]
	    [TestCase("192.168.0.1")]
	    public void GetExistsLinkByUserIpShouldBeNotNull(string userIp)
	    {
		    var links = _linkService.GetByUserIp(userIp);
		    Assert.That(links.Count, Is.EqualTo(1));
	    }

	    [Test]
	    [TestCase("https://www.tinkoff.ru/", "192.168.0.1")]
	    public void CompressAndSaveLinkShouldBeSuccess(string originalLink, string userIp)
	    {
		    var compressedUrl = "sqaD";
			_linkCompressService.Compress(Arg.Is(originalLink)).Returns(compressedUrl);
		    var links = _linkService.CompressAndSaveLink(originalLink, userIp);
		    _linkRepository.Received()
			    .Add(Arg.Is<Link>(x => x.CountUsages == 0 && x.UserId == 1 && x.OriginalUrl == originalLink));
	    }
	}
}
