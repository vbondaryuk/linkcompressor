﻿using System.Collections.Generic;
using System.IO;
using LinkCompressor.Application.Mappers;
using LinkCompressor.Domain.Repositories;
using LinkCompressor.Infrastructure.Data;
using LinkCompressor.Infrastructure.Data.Repositories;
using LinkCompressor.Infrastructure.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

namespace LinkCompressor.Web
{
  public class Startup
  {
    public Startup(IHostingEnvironment env)
    {
      var builder = new ConfigurationBuilder()
        .SetBasePath(env.ContentRootPath)
        .AddJsonFile("appsettings.json", false, true)
        .AddJsonFile($"appsettings.{env.EnvironmentName}.json", true)
        .AddEnvironmentVariables();
      Configuration = builder.Build();
    }

    public IConfigurationRoot Configuration { get; }

    public void ConfigureServices(IServiceCollection services)
    {
      // Add framework services.
      services.AddMvc();
      var sqlConnectionString = Configuration["ConnectionStrings:DataAccessPostgreSqlProvider"];

      services.AddDbContext<LinkCompressorContext>(options =>
        options.UseNpgsql(sqlConnectionString)
      );

      services.AddScoped<ILinkRepository, LinkRepository>();
      services.AddScoped<ILinkCompressService, LinkCompressorService>();
      services.AddScoped<IUserRepository, UserRepository>();
      services.AddScoped<ILinkMapper, LinkMapper>();
      services.AddScoped<ILinkService, LinkService>();
    }

    public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
    {
      app.Use(async (context, next) =>
      {
        await next();
        if (context.Response.StatusCode == 404 &&
            !Path.HasExtension(context.Request.Path.Value) &&
            !context.Request.Path.Value.StartsWith("/api/"))
        {
          context.Request.Path = "/index.html";
          await next();
        }
      });

      app.UseDefaultFiles(new DefaultFilesOptions {DefaultFileNames = new List<string> {"index.html"}});
      app.UseStaticFiles();
      app.UseMvc();
      loggerFactory.AddConsole(Configuration.GetSection("Logging"));
      loggerFactory.AddDebug();

      app.UseMvc();
      using (var context = app.ApplicationServices.GetService(typeof(LinkCompressorContext)) as LinkCompressorContext)
      {
        context.Database.Migrate();
      }      
    }
  }
}
