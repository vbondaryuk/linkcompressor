﻿import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CompressComponent } from './components/compress.component';
import { RedirectComponent } from './components/redirect.component';
import { LinkListComponent } from './components/link.component';

const appRoutes: Routes = [
  { path: '', component: CompressComponent },
  { path: "links", component: LinkListComponent },
  { path: "**", component: RedirectComponent }
];

export const routing: ModuleWithProviders = RouterModule.forRoot(appRoutes);