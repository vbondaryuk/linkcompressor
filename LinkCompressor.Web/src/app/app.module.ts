﻿import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { routing } from './app.routes';

import { AppComponent } from './app.component';
import { RedirectComponent } from './components/redirect.component';
import { LinkListComponent } from './components/link.component';
import { CompressComponent } from './components/compress.component';

import { LinkService } from './core/services/link.service';

@NgModule({
  declarations: [
    AppComponent,
    LinkListComponent,
    RedirectComponent,
    CompressComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    routing
  ],
  providers: [ 
    LinkService
  ],
  bootstrap: [AppComponent]
})

export class AppModule { }