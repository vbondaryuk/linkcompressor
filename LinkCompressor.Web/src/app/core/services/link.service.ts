﻿import { Injectable } from '@angular/core';
import { Http, Response, RequestOptions, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/throw';

import { ILink } from '../domain/ILink'

@Injectable()
export class LinkService {
    headers: Headers;

    constructor(private http: Http) { 
        this.http = http;
        this.headers = new Headers();
        this.headers.append('Content-Type', 'application/json');
    }

    getLinks(): Observable<ILink[]> {
        return this.http.get('api/link/')
                .map((response: Response) => <ILink[]> response.json())
                .catch(this.handleError);
    }

    compressLink(link: string) {
        let options = new RequestOptions({headers: this.headers});
        return this.http.post('api/link/compress', JSON.stringify({ link: link }), options)
                .map((res:Response) => res.text())
                .catch(this.handleError);
    }

    getLink(compressedLink: string) {
        let queryAddress = 'api/link/' + compressedLink;
        console.log(queryAddress);
        return this.http.get(queryAddress)
                .map((response: Response) => <ILink> response.json())
                .catch(this.handleError);
    }

    private handleError(error: Response) {
        console.error(error);
        return Observable.throw(error.json().error || 'Server error');
    }
}