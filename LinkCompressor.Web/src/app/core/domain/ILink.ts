﻿export interface ILink {
  originalUrl: string;
  shortUrl: string;
  creationDate: Date;
  countUsages: number;
}