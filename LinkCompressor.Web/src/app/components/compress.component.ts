﻿import { Component, OnInit } from '@angular/core';
import { Http, Response } from '@angular/http';
import { LinkService } from '../core/services/link.service';
@Component({
  selector: 'link-compressor',
  templateUrl: './compress.component.html',
  styleUrls: ['./compress.component.css']
})
export class CompressComponent { 

  compressedUrl: string;
  
  constructor(private _linkService: LinkService) { }
  
  compressLink(originalLink: string){
    if(originalLink){
      this._linkService.compressLink(originalLink).subscribe((compressedUrl)=>{
        this.compressedUrl = window.location.origin+'/'+compressedUrl;
      });
    }
  }
  
  clearCompressedInput(){
    this.compressedUrl = null;
  }
}