import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ILink } from '../core/domain/ILink';
import { LinkService } from '../core/services/link.service';

@Component({
    selector: 'redirect',
    templateUrl: 'redirect.component.html',
    styleUrls: ["redirect.component.css"]
})

export class RedirectComponent implements OnInit {

    isErrorUrl: boolean;

    constructor(private _linkService: LinkService, private _router: Router) { 
        this.isErrorUrl = false;
    }

    ngOnInit(): void {
        let compressedLink = this._router.url.substring(1);
        this._linkService.getLink(compressedLink).subscribe((link: ILink) => {
            if(link){
                window.location.assign(link.originalUrl);
            }else{
                this.isErrorUrl = true;
            }                        
        });
    }
}