﻿import { Component, OnInit } from '@angular/core';
import { ILink } from '../core/domain/ILink';
import { LinkService } from '../core/services/link.service';

@Component({
  selector: 'link-list',
  templateUrl: 'link.component.html',
  styleUrls: ["link.component.css"]
})

export class LinkListComponent implements OnInit {
  errorMessage: string;

  links: ILink[];
  hostName: string;

  constructor(private _linkService: LinkService) {
      this.hostName = window.location.origin;
  }

  ngOnInit(): void {
    this._linkService.getLinks()
      .subscribe(links => {
          this.links = links
        },
        error => {
          this.errorMessage = <any>error
        });
  }
}