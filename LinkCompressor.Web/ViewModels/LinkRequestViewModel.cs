﻿namespace LinkCompressor.Web.ViewModels
{
  public class LinkRequestViewModel
  {
    public string Link { get; set; }
  }
}
