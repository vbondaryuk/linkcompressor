﻿using System.Collections.Generic;
using LinkCompressor.Application.Mappers;
using LinkCompressor.Application.ViewModels;
using LinkCompressor.Infrastructure.Services;
using LinkCompressor.Web.ViewModels;
using Microsoft.AspNetCore.Http.Features;
using Microsoft.AspNetCore.Mvc;

namespace LinkCompressor.Web.Controllers
{
  [Route("api/[controller]")]
  public class LinkController : Controller
  {
    private readonly ILinkMapper _linkMapper;
    private readonly ILinkService _linkService;

    public LinkController(ILinkService linkService, ILinkMapper linkMapper)
    {
      _linkService = linkService;
      _linkMapper = linkMapper;
    }

    [HttpGet]
    public IEnumerable<LinkViewModel> GetAll()
    {
      var ip = HttpContext.Features.Get<IHttpConnectionFeature>()?.RemoteIpAddress?.ToString();
      var links = _linkService.GetByUserIp(ip);
      return _linkMapper.Map(links);
    }

    [HttpGet("{shortUrl}")]
    public LinkViewModel GetByCompressedUrl(string shortUrl)
    {
      var link = _linkService.GetLinkAndIncreaseUsages(shortUrl);
      return link == null ? null : _linkMapper.Map(link);
    }

    [HttpPost]
    [Route("compress")]
    public string Compress([FromBody] LinkRequestViewModel linkRequestViewModel)
    {
      var ip = HttpContext.Features.Get<IHttpConnectionFeature>()?.RemoteIpAddress?.ToString();
      return _linkService.CompressAndSaveLink(linkRequestViewModel.Link, ip);
    }
  }
}
