﻿namespace LinkCompressor.Infrastructure.Services
{
   public interface ILinkCompressService
   {
      string Compress(string link);
   }
}