﻿using System;
using System.Collections.Generic;
using LinkCompressor.Domain.Models;
using LinkCompressor.Domain.Repositories;

namespace LinkCompressor.Infrastructure.Services
{
	public class LinkService : ILinkService
	{
		private readonly ILinkCompressService _linkCompressService;
		private readonly ILinkRepository _linkRepository;
		private readonly IUserRepository _userRepository;

		public LinkService(
			ILinkCompressService linkCompressService,
			ILinkRepository linkRepository,
			IUserRepository userRepository)
		{
			_linkCompressService = linkCompressService;
			_linkRepository = linkRepository;
			_userRepository = userRepository;
		}

		public string CompressAndSaveLink(string originalUrl, string userIp)
		{
			var compressedLink = _linkCompressService.Compress(originalUrl);
			var user = _userRepository.GetUserCreateIfNotExists(userIp);
			var link = new Link
			{
				CreationDate = DateTime.Now,
				OriginalUrl = originalUrl,
				UserId = user.Id,
				ShortUrl = compressedLink
			};
			_linkRepository.Add(link);
			_linkRepository.Commit();
			return compressedLink;
		}

		public IEnumerable<Link> GetByUserIp(string userIp)
		{
			var user = _userRepository.GetSingle(x => x.Ip == userIp, u => u.Links);
			if (user == null)
				yield break;
			foreach (var userLink in user.Links)
				yield return userLink;
		}

		public Link GetLinkAndIncreaseUsages(string compressedLink)
		{
			var link = _linkRepository.GetSingle(x => x.ShortUrl == compressedLink);
			if (link == null)
				return null;
			link.CountUsages++;
			_linkRepository.Edit(link);
			_linkRepository.Commit();
			return link;
		}
	}
}