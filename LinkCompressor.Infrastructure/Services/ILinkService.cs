﻿using System.Collections.Generic;
using LinkCompressor.Domain.Models;

namespace LinkCompressor.Infrastructure.Services
{
	public interface ILinkService
	{
		string CompressAndSaveLink(string originalUrl, string userIp);
		IEnumerable<Link> GetByUserIp(string userIp);
		Link GetLinkAndIncreaseUsages(string compressedLink);
	}
}