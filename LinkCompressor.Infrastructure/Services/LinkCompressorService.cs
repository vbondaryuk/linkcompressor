﻿using System;

namespace LinkCompressor.Infrastructure.Services
{
   public class LinkCompressorService : ILinkCompressService
   {
      public string Compress(string link)
      {
         var hash = link.GetHashCode() + DateTimeOffset.Now.UtcTicks;
         var bytes = BitConverter.GetBytes(hash);
         var compressedLink = Convert.ToBase64String(bytes, 0, 5).Substring(0, 6);
         return compressedLink;
      }
   }
}