﻿using LinkCompressor.Domain.Models;
using Microsoft.EntityFrameworkCore;

namespace LinkCompressor.Infrastructure.Data
{
   public class LinkCompressorContext : DbContext
	{
      public DbSet<User> Users { get; set; }
      public DbSet<Link> Links { get; set; }

      public LinkCompressorContext(DbContextOptions<LinkCompressorContext> options) : base(options)
      {
      }

      protected override void OnModelCreating(ModelBuilder modelBuilder)
      {
         modelBuilder.Entity<User>().Property(a => a.Ip).IsRequired();
         modelBuilder.Entity<User>().HasMany(a => a.Links).WithOne(p => p.User);

         modelBuilder.Entity<Link>().Property(u => u.OriginalUrl).IsRequired();
         modelBuilder.Entity<Link>().Property(u => u.ShortUrl).IsRequired().HasMaxLength(10);
         modelBuilder.Entity<Link>().Property(u => u.CreationDate).IsRequired();
         modelBuilder.Entity<Link>().Property(u => u.UserId).IsRequired();
         modelBuilder.Entity<Link>().HasIndex(b => b.ShortUrl);         
      }
   }
}