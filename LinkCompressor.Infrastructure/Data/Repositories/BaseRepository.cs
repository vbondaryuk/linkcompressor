﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using LinkCompressor.Domain.Models;
using LinkCompressor.Domain.Repositories;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;

namespace LinkCompressor.Infrastructure.Data.Repositories
{
   public abstract class BaseRepository<T> : IRepository<T> where T : BaseEntity, new()
   {
      private readonly LinkCompressorContext _context;

      protected BaseRepository(LinkCompressorContext context)
      {
         _context = context;
      }

      public IEnumerable<T> GetAll()
      {
         return _context.Set<T>().AsEnumerable();
      }

      public async Task<IEnumerable<T>> GetAllAsync()
      {
         return await _context.Set<T>().ToListAsync();
      }

      public IEnumerable<T> AllIncluding(params Expression<Func<T, object>>[] includeProperties)
      {
         IQueryable<T> query = _context.Set<T>();
         foreach (var includeProperty in includeProperties)
         {
            query = query.Include(includeProperty);
         }
         return query.AsEnumerable();
      }

      public T GetSingle(Expression<Func<T, bool>> predicate)
      {
         return _context.Set<T>().FirstOrDefault(predicate);
      }

      public T GetSingle(Expression<Func<T, bool>> predicate, params Expression<Func<T, object>>[] includeProperties)
      {
         IQueryable<T> query = _context.Set<T>();
         foreach (var includeProperty in includeProperties)
         {
            query = query.Include(includeProperty);
         }

         return query.Where(predicate).FirstOrDefault();
      }

      public virtual IEnumerable<T> FindBy(Expression<Func<T, bool>> predicate)
      {
         return _context.Set<T>().Where(predicate);
      }

      public void Add(T entity)
      {
         _context.Set<T>().Add(entity);
      }

      public virtual void Edit(T entity)
      {
	      _context.Set<T>().Update(entity);
      }
      public virtual void Delete(T entity)
      {
         EntityEntry dbEntityEntry = _context.Entry(entity);
         dbEntityEntry.State = EntityState.Deleted;
      }

      public virtual void Commit()
      {
         _context.SaveChanges();
      }
   }
}