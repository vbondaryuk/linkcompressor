﻿using LinkCompressor.Domain.Models;
using LinkCompressor.Domain.Repositories;

namespace LinkCompressor.Infrastructure.Data.Repositories
{
   public class LinkRepository : BaseRepository<Link>, ILinkRepository
   {
      public LinkRepository(LinkCompressorContext context) : base(context)
      {
      }
   }
}