﻿using System.Collections.Generic;
using LinkCompressor.Domain.Models;
using LinkCompressor.Domain.Repositories;

namespace LinkCompressor.Infrastructure.Data.Repositories
{
   public class UserRepository : BaseRepository<User>, IUserRepository
   {
      public UserRepository(LinkCompressorContext context) : base(context)
      {
      }

      public User GetUserCreateIfNotExists(string userIp)
      {
         var user = GetSingle(x => x.Ip == userIp);
         if (user == null)
         {
            user = new User{Ip = userIp};
            Add(user);
            Commit();
         }
         return user;
      }

      public IEnumerable<Link> GetUserLinks(string userIp)
      {
         var user = GetSingle(x => x.Ip == userIp);
         if (user == null)
            yield break;
         foreach (var userLink in user.Links)
         {
            yield return userLink;
         }
      }
   }
}