﻿using System.Collections.Generic;

namespace LinkCompressor.Domain.Models
{
   public class User : BaseEntity
   {
      public User()
      {
         Links = new List<Link>();
      }
      public string Ip { get; set; }
      public virtual ICollection<Link> Links { get; set; }
   }
}