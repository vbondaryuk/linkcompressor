﻿using System;

namespace LinkCompressor.Domain.Models
{
   public class Link : BaseEntity
   {
      public int UserId { get; set; }
      public string OriginalUrl { get; set; }
      public string ShortUrl { get; set; }
      public DateTime CreationDate { get; set; }
      public int CountUsages { get; set; }
      public virtual User User { get; set; }
   }
}