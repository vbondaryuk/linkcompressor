﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using LinkCompressor.Domain.Models;

namespace LinkCompressor.Domain.Repositories
{
   public interface IRepository<T> where T: BaseEntity, new()
   {
      IEnumerable<T> GetAll();
      T GetSingle(Expression<Func<T, bool>> predicate);
      T GetSingle(Expression<Func<T, bool>> predicate, params Expression<Func<T, object>>[] includeProperties);
      IEnumerable<T> FindBy(Expression<Func<T, bool>> predicate);
      void Add(T entity);
      void Delete(T entity);
      void Edit(T entity);
      void Commit();
   }
}