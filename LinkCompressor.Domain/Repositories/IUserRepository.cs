﻿using System.Collections.Generic;
using LinkCompressor.Domain.Models;

namespace LinkCompressor.Domain.Repositories
{
   public interface IUserRepository : IRepository<User>
   {
      User GetUserCreateIfNotExists(string userIp);
      IEnumerable<Link> GetUserLinks(string userIp);
   }
}