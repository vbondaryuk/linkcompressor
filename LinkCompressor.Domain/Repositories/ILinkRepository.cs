﻿using LinkCompressor.Domain.Models;

namespace LinkCompressor.Domain.Repositories
{
   public interface ILinkRepository : IRepository<Link>
   {
      
   }
}