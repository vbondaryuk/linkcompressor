﻿using System.Collections.Generic;
using System.Linq;
using LinkCompressor.Application.ViewModels;
using LinkCompressor.Domain.Models;

namespace LinkCompressor.Application.Mappers
{
	public class LinkMapper : ILinkMapper
	{
		public IEnumerable<LinkViewModel> Map(IEnumerable<Link> links)
		{
			return links.Select(Map);
		}

		public LinkViewModel Map(Link link)
		{
			if (link == null)
				return null;	
			return new LinkViewModel
			{
				CreationDate = link.CreationDate,
				OriginalUrl = link.OriginalUrl,
				ShortUrl = link.ShortUrl,
				CountUsages = link.CountUsages
			};
		}
	}
}