﻿using System.Collections.Generic;
using LinkCompressor.Application.ViewModels;
using LinkCompressor.Domain.Models;

namespace LinkCompressor.Application.Mappers
{
	public interface ILinkMapper
	{
		IEnumerable<LinkViewModel> Map(IEnumerable<Link> links);
		LinkViewModel Map(Link link);
	}
}