﻿using System;

namespace LinkCompressor.Application.ViewModels
{
   public class LinkViewModel
   {
      public string OriginalUrl { get; set; }
      public string ShortUrl { get; set; }
      public DateTime CreationDate { get; set; }
	  public int CountUsages { get; set; }
   }
}